#!/bin/bash

F=`pwd`
apt-get update -y ; apt-get install -y wget curl fakeroot 
wget https://download.sonatype.com/nexus/3/latest-unix.tar.gz

mkdir -p nexus/opt

tar -zxf latest-unix.tar.gz --directory nexus/opt/

cd nexus/opt
V=`ls | grep nexus | sed -e "s/nexus-//g"`
echo "Liste:"
ls -d nexus-*
echo "Ende"
ln -s `ls -d nexus-*` nexus

sed -i $F/nexus/DEBIAN/control -e "s/Version:.*/Version: $V/"

cd $F

if [[ -f latest-unix.tar.gz ]]
then
   echo "Removing latest-unix.tar.gz" && rm latest-unix.tar.gz
fi
chmod 755 nexus/DEBIAN
chmod 644 nexus/DEBIAN/*
chmod 755 nexus/DEBIAN/postinst
#chmod 755 nexus/DEBIAN/prerm
  

fakeroot dpkg -b nexus nexus_$V.deb

